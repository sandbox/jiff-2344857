<?php

/**
 * Implementation of hook_rules_event_info().
 */
function entity_verification_code_rules_event_info() {
  $items = array(
    'entity_verification_code_entity_is_verified' => array(
      'label' => t('After an entity is verified'),
      'category' => 'node',
      'module' => 'entity_verification_code',
//      'variables' => entity_rules_en
    ),
    'node_verify_verification_expired' => array(
      'label' => t('After a node verification expires'),
      'category' => 'node',
      'module' => 'node_verify',
      'variables' => rules_events_node_variables(t('Verified Node')),
    ),
  );
  return $items;
}