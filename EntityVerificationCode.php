<?php

class EntityVerificationCode {

  const EVC_VERIFIED = 1;
  const EVC_UNVERIFIED = 0;
  const EVC_EXPIRED = -1;

  protected static $table = 'entity_verification_code';
  protected static $alias = 'evc';

  public $entityType;
  public $entityId;

  function __construct() {

  }

  public static function load($entity_type, $entity_id) {
    $evc = new EntityVerificationCode();
    $evc->set('entityType', $entity_type);
    $evc->set('entityId', $entity_id);
    return $evc;
  }

  public static function loadAllExpired() {
    $expired_entities = db_select(self::$table, self::$alias)
      ->fields(self::$alias, array('entity_type', 'id'))
      ->condition('expiration', time(), '<=')
      ->condition('status', self::EVC_UNVERIFIED)
      ->execute();

    $entity_verification_list = array();
    while($expired_entity = $expired_entities->fetchAssoc()) {
      $id = $expired_entity['id'];
      $entity_verification_list[$expired_entity['entity_type']][$id] = $id;
    }

    return self::loadMultiple($entity_verification_list);
  }

  protected static function loadMultiple(array $entity_list) {
    // Base query.
    $query = db_select(self::$table, self::$alias)
      ->fields(self::$alias, array());

    // Loop through each entity type and add in related conditions.
    $or = db_or();
    foreach($entity_list as $entity_type => $entity_ids) {
      $and = db_and()
        ->condition('entity_type', $entity_type)
        ->condition('id', $entity_ids);

      // Since we are dealing with multiple sets of entities we need to put
      // each entity type's conditions into an OR statement.
      $or->condition($and);
    }

    $result = $query->condition($or)->execute();

    // Load up EntityVerificationCode objects for each result.
    $evc_list = array();
    while($evc = $result->fetchAssoc()) {
      $evc_list[] = self::load($evc['entity_type'], $evc['id']);
    }

    return $evc_list;
  }

  protected function set($property, $value) {
    $this->{$property} = $value;
  }
}